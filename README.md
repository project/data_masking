# Introduction

Data masking is a way to create a fake, but a realistic version of your organizational data. The goal is to protect sensitive data, while providing a functional alternative when real data is not needed—for example, in user training, demos, or software testing.

This module provides functions to update value of entity's fields by existing or you own Data masking plugins.

The existing Data Masking Plugins are in module `data_masking_example`

## Create Data Masking Plugin

1. In you custom module, create folder `src/Plugin/DataMaskingPlugin`
2. Create Class extends `DataMaskingPluginBase`, Ex. TextMasking.php
3. Use annotation `@DataMaskingPlugin` for created class, view description in `src/Annotation/DataMaskingPlugin.php`
4. Create functions that map with `masking_schemes` in Annotation
5. Clear cache

## Data Masking

1. Go to `/admin/data-masking/form/batch`
2. Select Entity Type
3. Select Bundle
4. Select Field (only show fields that have supporting Data Masking Plugin)
5. Select Property of Field
6. Select Masking Method
7. Select Masking Scheme
8. Enter Batch Size (optional, min:1, max:100)
9. Click `Data Masking`

## Data Masking Multiple Fields in onetime
1. Go to `/admin/data-masking-multiple/form/batch`
2. Download example csv file below `Masking Config File` field
3. Update data in downloaded CSV, about values in CSV files, you can refer by select fields in `/admin/data-masking/form/batch` and view the select id in each fields
4. Upload the updated CSV and click `Override System With The Upload File` to upload file
5. The last uploaded file is always rendered as the table on form, you can click on `Download Masking Config file` to download the latest uploaded CSV
6. Enter Batch Size (optional, min:1, max:100)
7. Click `Data Masking`
