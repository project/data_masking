<?php

namespace Drupal\data_masking_example\Plugin\DataMaskingPlugin;

use Drupal\data_masking\Plugin\DataMaskingPluginBase;

/**
 * Class email masking method.
 *
 * @DataMaskingPlugin(
 *   id = "masking_email",
 *   label = "Masking Email",
 *   allowed_field_types = {
 *      "email",
 *      "string",
 *   },
 *   masking_schemes = {
 *      "mailinator" = "Mailinator",
 *   }
 * )
 */
class MailinatorMasking extends DataMaskingPluginBase {

  /**
   * Masking Scheme: Mailinator.
   */
  protected function mailinator($value, $context) {
    if (!empty($value) && isset($context['entity'])) {
      $entity = $context['entity'];
      return "mail_{$entity->getEntityTypeId()}_{$entity->id()}@mailinator.com";
    }
    return $value;
  }

}
