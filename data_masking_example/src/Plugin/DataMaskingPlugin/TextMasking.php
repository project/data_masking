<?php

namespace Drupal\data_masking_example\Plugin\DataMaskingPlugin;

use Drupal\data_masking\Plugin\DataMaskingPluginBase;

/**
 * Class Text masking method.
 *
 * @DataMaskingPlugin(
 *   id = "masking_text",
 *   label = "Masking Text",
 *   allowed_field_types = {
 *      "string",
 *   },
 *   masking_schemes = {
 *      "xInMiddle" = "X In Middle",
 *      "randomNumberInMiddle" = "Random Number In Middle",
 *   }
 * )
 */
class TextMasking extends DataMaskingPluginBase {

  /**
   * Masking Scheme: X In Middle.
   */
  protected function xInMiddle($value, $context) {
    if (!empty($value) && mb_strlen($value) > 2) {
      $start = mb_substr($value, 0, 1);
      $end = mb_substr($value, -1, 1);
      $middle = str_repeat('x', mb_strlen($value) - 2);
      return "$start$middle$end";
    }
    return $value;
  }

  /**
   * Masking Scheme: Random Number In Middle.
   */
  protected function randomNumberInMiddle($value, $context) {
    if (!empty($value) && mb_strlen($value) > 2) {
      $start = mb_substr($value, 0, 1);
      $end = mb_substr($value, -1, 1);
      $max = str_repeat('9', mb_strlen($value) - 2);
      $rand = mt_rand(0, (int) $max);
      $middle = str_pad($rand, mb_strlen($value) - 2, '0', STR_PAD_LEFT);
      return "$start$middle$end";
    }
    return $value;
  }

}
