<?php

namespace Drupal\data_masking_example\Plugin\DataMaskingPlugin;

use Drupal\data_masking\Plugin\DataMaskingPluginBase;

/**
 * Class Date masking method.
 *
 * @DataMaskingPlugin(
 *   id = "masking_date",
 *   label = "Masking Date",
 *   allowed_field_types = {
 *      "datetime",
 *   },
 *   masking_schemes = {
 *      "randomDateMonth" = "Random Date Month",
 *   }
 * )
 */
class DateMasking extends DataMaskingPluginBase {

  /**
   * Masking Scheme: Random Date Month.
   */
  protected function randomDateMonth($value, $context) {
    if (!empty($value)) {
      $year = \DateTime::createFromFormat('Y-m-d', $value)->format('Y');
      if ($year) {
        $start = \DateTime::createFromFormat('Y-m-d', "{$year}-01-01")
          ->getTimestamp();
        $end = \DateTime::createFromFormat('Y-m-d', "{$year}-12-31")
          ->getTimestamp();
        $newTimestamp = mt_rand($start, $end);
        return date('Y-m-d', $newTimestamp);
      }
    }
    return $value;
  }

}
