<?php

namespace Drupal\data_masking_example\Plugin\DataMaskingPlugin;

use Drupal\data_masking\Plugin\DataMaskingPluginBase;

/**
 * Class Phone Number masking method.
 *
 * @DataMaskingPlugin(
 *   id = "masking_phone_number",
 *   label = "Masking Phone Number",
 *   allowed_field_types = {
 *      "string",
 *      "telephone",
 *   },
 *   masking_schemes = {
 *      "randomNumberInMiddle" = "Random Number In Middle",
 *   }
 * )
 */
class PhoneNumberMasking extends DataMaskingPluginBase {

  /**
   * Masking Scheme: Random Number In Middle.
   */
  protected function randomNumberInMiddle($value, $context) {
    $value = trim($value);
    if (!empty(trim($value)) && isset($context['entity'])) {
      if (mb_substr($value, 0, 1) == '+') {
        $startLength = 4;
      }
      else {
        $startLength = 1;
      }
      $value_len = mb_strlen($value);
      if ($value_len > $startLength + 2) {
        // ALWAYS PADDING 0.
        $start = mb_substr($value, 0, $startLength) . "0";
        $end = mb_substr($value, -1, 1);
        $randLength = $value_len - ($startLength + 2);
        $max = str_repeat('9', $randLength);
        $rand = mt_rand(0, (int) $max);
        $middle = str_pad($rand, $randLength, '0', STR_PAD_LEFT);
        return "$start$middle$end";
      }
    }
    return $value;
  }

}
