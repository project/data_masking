<?php

namespace Drupal\data_masking;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\data_masking\Plugin\DataMaskingPluginManager;

/**
 * Data masking Service.
 *
 * @package Drupal\data_masking
 */
class DataMaskingService {

  const ENTITY_TYPE = 'Entity Type';
  const BUNDLE = 'Bundle';
  const FIELD = 'Field';
  const PROPERTY = 'Property';
  const MASKING_METHOD = 'Masking Method';
  const MASKING_SCHEME = 'Masking Scheme';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity definition update manager service.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * String translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translation;

  /**
   * Logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager, TranslationInterface $translation, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->translation = $translation;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * Build masking entity data batch.
   */
  public function buildBatch($batch_size, $masking_config) {
    $group_config = [];
    $total_count = 0;
    foreach ($masking_config as $value) {
      $group_config[$value[self::ENTITY_TYPE]][$value[self::BUNDLE]][] = $value;
    }
    $operations = [];
    foreach ($group_config as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $data) {
        $entity_query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
        $entity_type_load = $this->entityDefinitionUpdateManager->getEntityType($entity_type);
        $entity_keys = $entity_type_load->getKeys();
        $bundle_type = $entity_keys['bundle'];
        if ($bundle_type) {
          $entity_query->condition($bundle_type, $bundle);
        }
        $entity_count = $entity_query->count()->execute();
        $total_count += $entity_count;
        if (!empty($entity_count)) {
          for ($i = 0; $i <= ($entity_count / $batch_size); $i++) {
            $operations[] = [
              '\Drupal\data_masking\DataMaskingService::processEntity',
              [
                $i,
                $batch_size,
                $entity_type,
                $bundle,
                $data,
              ],
            ];
          }
        }
      }
    }

    if (!empty($operations)) {
      $total_operation = count($operations);
      foreach ($operations as $operation_id => &$operation) {
        $operation[1][] = $operation_id + 1;
        $operation[1][] = $total_operation;
      }
      return [
        'title' => "Data Masking for Entity",
        'operations' => $operations,
        'init_message' => $this->translation->formatPlural($total_count, 'Processing 1 record.', 'Processing @count records.'),
        'finished' => '\Drupal\data_masking\DataMaskingService::processFinish',
        'entity_count' => $total_count,
      ];
    }
    return [];
  }

  /**
   * ProcessItem.
   */
  public static function processEntity($step, $batch_size, $entity_type, $bundle, $data, $operation_id, $total_operation, &$context) {
    set_time_limit(0);
    $data_masking_plugin_service = \Drupal::service('plugin.manager.data_masking_plugin');
    $entity_query = \Drupal::entityQuery($entity_type);
    $manager = \Drupal::entityDefinitionUpdateManager();
    $entity_type_load = $manager->getEntityType($entity_type);
    $entity_keys = $entity_type_load->getKeys();
    $bundle_type = $entity_keys['bundle'];
    if ($bundle_type) {
      $entity_query->condition($bundle_type, $bundle);
    }
    $entity_ids = $entity_query
      ->range($step * $batch_size, $batch_size)
      ->execute();

    $context['results'][] = count($entity_ids);

    foreach ($entity_ids as $id) {
      try {
        $entities = \Drupal::entityTypeManager()
          ->getStorage($entity_type)
          ->loadByProperties([
            $entity_keys['id'] => $id,
          ]);
        if (!empty($entities)) {
          $entity = reset($entities);
          // Masking Entity.
          self::maskingEntity($data_masking_plugin_service, $entity, $data);
        }
        else {
          throw new \Exception("Not Found Entity!");
        }
      }
      catch (\Exception $ex) {
        \Drupal::logger('data_masking_batch')
          ->error("Cannot masking entity has id=$id (Data Masking for Entity: $entity_type, Bundle: $bundle, Error: " . $ex->getMessage());
      }
    }
    if ($operation_id % 10 == 0 || $operation_id == $total_operation) {
      \Drupal::logger('data_masking_batch')
        ->info("Processed page {$operation_id}/$total_operation");
    }
  }

  /**
   * ProcessFinish.
   */
  public static function processFinish($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      $count = array_sum($results);
      $messenger->addMessage(\Drupal::translation()->formatPlural(
        $count, '1 result processed.', '@count results processed.')
      );
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * Masking an Entity.
   *
   * @param \Drupal\data_masking\Plugin\DataMaskingPluginManager $data_masking_plugin_service
   *   Data masking plugin service.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to mask data.
   * @param array $data
   *   Prepared data to mask data.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private static function maskingEntity(DataMaskingPluginManager $data_masking_plugin_service, EntityInterface $entity, array $data) {
    if (is_callable([$entity, 'setIsUpdateFromSync'])) {
      $entity->setIsUpdateFromSync(TRUE);
    }
    foreach ($data as $row) {
      $field = $row[self::FIELD];
      $property = $row[self::PROPERTY];
      $masking_method = $row[self::MASKING_METHOD];
      $masking_scheme = $row[self::MASKING_SCHEME];
      $context = [
        'entity' => $entity,
        'data_masking' => $row,
      ];

      if ($entity->{$field}) {
        $masking_plugin = $data_masking_plugin_service->createInstance($masking_method);

        $values = $entity->{$field}->getValue();
        foreach ($values as $delta => $properties) {
          if (isset($properties[$property])) {
            if (empty($masking_scheme)) {
              $masking_scheme = '';
            }
            $values[$delta][$property] = $masking_plugin->masking($properties[$property], $context, $masking_scheme);
          }
          else {
            throw new \Exception('Not Found Property!');
          }
        }
        $entity->{$field}->setValue($values);
      }
    }
    $entity->save();
  }

  /**
   * Validate masking csv file.
   */
  public function validateMaskingConfigFile($fileUri) {
    // Open CSV file.
    $handle = fopen($fileUri, 'r');

    try {
      // Get the column names.
      $column_names = fgetcsv($handle);
      foreach ($column_names as $index => $name) {
        $column_names[$index] = $name;
      }
      $header = [
        self::ENTITY_TYPE,
        self::BUNDLE,
        self::FIELD,
        self::PROPERTY,
        self::MASKING_METHOD,
        self::MASKING_SCHEME,
      ];
      if (count(array_intersect($header, $column_names)) != 6) {
        fclose($handle);
        $headerText = implode('|', $header);
        $this->messenger->addError("The uploaded file does not have the correct structure: {$headerText} !");
        return FALSE;
      }
    }
    catch (\Exception $ex) {
      $this->messenger->addError($ex->getMessage());
      fclose($handle);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Read masking csv to array.
   */
  public function readCsv($fileUri) {
    $contents = file_get_contents($fileUri);
    $contents = preg_replace('~\R~u', "\r\n", $contents);
    file_put_contents($fileUri, $contents);

    $records = [];

    // Open CSV file.
    $handle = fopen($fileUri, 'r');

    try {
      // Get the column names.
      $column_names = fgetcsv($handle);
      foreach ($column_names as $index => $name) {
        $column_names[$index] = $name;
      }

      while (!feof($handle)) {
        $values = fgetcsv($handle);
        // Complete ignored empty rows.
        if (empty($values) || $values == [''] || empty(trim(implode("", $values)))) {
          continue;
        }

        $record = array_combine($column_names, $values);
        foreach ($record as $key => $value) {
          $record[$key] = trim($value);
        }
        $records[] = $record;
      }
      fclose($handle);
    }
    catch (\Exception $ex) {
      $this->messenger->addError($ex->getMessage());
      fclose($handle);
    }

    return $records;
  }

}
