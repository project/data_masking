<?php

namespace Drupal\data_masking\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Data masking plugin plugins.
 */
interface DataMaskingPluginInterface extends PluginInspectionInterface {

  /**
   * Data masking function.
   *
   * @param mixed $value
   *   Original value to masking data.
   * @param array $context
   *   Context values to masking data.
   *   elements:
   *   - 'entity' Entity to mask data.
   *   - 'data_masking' Inputted data of current entity in masking form.
   * @param string $scheme
   *   Scheme to masking data.
   *
   * @return mixed
   *   Masked data.
   */
  public function masking(mixed $value, array $context, string $scheme = '');

}
