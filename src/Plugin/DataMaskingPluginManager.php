<?php

namespace Drupal\data_masking\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Data masking plugin plugin manager.
 */
class DataMaskingPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new DataMaskingPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DataMaskingPlugin', $namespaces, $module_handler, 'Drupal\data_masking\Plugin\DataMaskingPluginInterface', 'Drupal\data_masking\Annotation\DataMaskingPlugin');

    $this->alterInfo('data_masking_data_masking_plugin_info');
    $this->setCacheBackend($cache_backend, 'data_masking_data_masking_plugin_plugins');
  }

  /**
   * Return Definition by allowed field type.
   */
  public function getDefinitionByAllowedFieldType($field_type) {
    $definitions = parent::getDefinitions();
    return array_filter($definitions, function ($definition) use ($field_type) {
      return in_array($field_type, $definition['allowed_field_types']);
    });
  }

  /**
   * Get all existing masking field types.
   */
  public function getExistingMaskingFieldTypes() {
    $definitions = parent::getDefinitions();
    $existingFieldTypes = [];
    foreach ($definitions as $definition) {
      foreach ($definition['allowed_field_types'] as $allowed_field_type) {
        if (!isset($existingFieldTypes[$allowed_field_type])) {
          $existingFieldTypes[$allowed_field_type] = $allowed_field_type;
        }
      }
    }
    return $existingFieldTypes;
  }

}
