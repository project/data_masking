<?php

namespace Drupal\data_masking\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Data masking plugin plugins.
 */
abstract class DataMaskingPluginBase extends PluginBase implements DataMaskingPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function masking(mixed $value, array $context, string $scheme = '') {
    if (!empty($scheme) && is_callable([$this, $scheme])) {
      return call_user_func([$this, $scheme], $value, $context);
    }
    return $value;
  }

}
