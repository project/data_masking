<?php

namespace Drupal\data_masking\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a data masking plugin item annotation object.
 *
 * @see \Drupal\data_masking\Plugin\DataMaskingPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class DataMaskingPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The field types that allow masking.
   *
   * @var array
   */
  public array $allowed_field_types = [];

  /**
   * The masking schemes, map with function inside class.
   *
   * @var array
   */
  public array $masking_schemes = [];

}
