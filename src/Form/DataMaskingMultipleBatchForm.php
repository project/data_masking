<?php

namespace Drupal\data_masking\Form;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\data_masking\DataMaskingService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data Masking Multiple Fields Batch Form.
 */
class DataMaskingMultipleBatchForm extends DataMaskingBatchForm {

  /**
   * The masking data service.
   *
   * @var \Drupal\data_masking\DataMaskingService
   */
  protected $dataMaskingService;

  /**
   * The extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataMaskingService $dataMaskingService, ExtensionPathResolver $extensionPathResolver, FileUrlGeneratorInterface $fileUrlGenerator) {
    $this->dataMaskingService = $dataMaskingService;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('data_masking.service'),
      $container->get('extension.path.resolver'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'data_masking_multiple_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $exampleFileUri = $this->extensionPathResolver->getPath('module', 'data_masking') . '/assets/data_masking.csv';
    $form['masking_file'] = [
      '#type' => 'file',
      '#title' => $this->t('Masking Config File'),
      '#description' => $this->t('CSV format only, <a href=":download_url">Example</a>', [':download_url' => $this->fileUrlGenerator->generateAbsoluteString($exampleFileUri)]),
    ];

    $maskingFileUri = 'private://data_masking/uploaded_masking_config.csv';
    if (file_exists($maskingFileUri)) {
      $records = $this->dataMaskingService->readCsv($maskingFileUri);
      $form['masking_config'] = [
        '#type' => 'table',
        '#header' => [
          '#',
          DataMaskingService::ENTITY_TYPE,
          DataMaskingService::BUNDLE,
          DataMaskingService::FIELD,
          DataMaskingService::PROPERTY,
          DataMaskingService::MASKING_METHOD,
          DataMaskingService::MASKING_SCHEME,
        ],
        '#caption' => $this->t('<a href=":download_url">Download Masking Config file</a>', [':download_url' => $this->fileUrlGenerator->generateAbsoluteString($maskingFileUri)]),
      ];
      foreach ($records as $i => $record) {
        $form['masking_config'][$i]['id'] = [
          '#plain_text' => (string) ($i + 1),
        ];
        foreach ($record as $key => $value) {
          $form['masking_config'][$i][$key] = [
            '#plain_text' => $value,
          ];
        }
      }
    }
    else {
      \Drupal::messenger()->addError('Masking Config File Is Not Found!');
    }

    $form['batch_size'] = [
      '#type' => 'number',
      '#max' => 100,
      '#min' => 1,
      '#default_value' => 20,
      '#required' => TRUE,
      '#title' => $this->t('Batch size'),
    ];

    $form['actions']['update_masking_config_file'] = [
      '#type' => 'submit',
      '#value' => 'Override System With The Upload File',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Data Masking',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $files = $this->getRequest()->files->get('files', []);
    if (!empty($files['masking_file']) && !$files['masking_file']->isValid()) {
      $form_state->setErrorByName('csv', $this->t('The file could not be uploaded.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    if ($form_state->getValue('op') == 'Override System With The Upload File') {
      $fileSystem = \Drupal::service('file_system');
      $directory = "private://data_masking/";
      $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

      $validators = ['file_validate_extensions' => ['csv']];
      $uploadedFiles = file_save_upload(
        'masking_file',
        $validators,
        'private://data_masking/',
        NULL,
        FileSystemInterface::EXISTS_REPLACE
      );
      foreach ($uploadedFiles as $uploadedFile) {
        if ($this->dataMaskingService->validateMaskingConfigFile($uploadedFile->getFileUri())) {
          \Drupal::service('file.repository')->move(
            $uploadedFile,
            'private://data_masking/uploaded_masking_config.csv',
            FileSystemInterface::EXISTS_REPLACE
          );
        }
        else {
          \Drupal::entityTypeManager()
            ->getStorage('file')
            ->delete([$uploadedFile]);
        }
      }
      $messenger->addMessage($this->t('Uploaded Masking Config File!'));
    }
    elseif ($form_state->getValue('op') == 'Data Masking') {
      $maskingFileUri = 'private://data_masking/uploaded_masking_config.csv';
      $batch_size = $form_state->getValue('batch_size');
      if (file_exists($maskingFileUri)) {

        $records = $this->dataMaskingService->readCsv($maskingFileUri);
        $batch = $this->dataMaskingService->buildBatch($batch_size, $records);
        if (empty($batch)) {
          $messenger->addMessage("Not found entity to process!");
        }
        else {
          batch_set($batch);
        }
      }
      else {
        \Drupal::messenger()->addError('Masking Config File Is Not Found!');
      }
    }
  }

}
