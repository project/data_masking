<?php

namespace Drupal\data_masking\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\data_masking\DataMaskingService;
use Drupal\data_masking\Plugin\DataMaskingPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data Masking Batch Form.
 */
class DataMaskingBatchForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\data_masking\Plugin\DataMaskingPluginManager
   */
  protected $dataMaskingPluginManager;

  /**
   * The masking data service.
   *
   * @var \Drupal\data_masking\DataMaskingService
   */
  protected $dataMaskingService;

  /**
   * Entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'data_masking_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, DataMaskingPluginManager $dataMaskingPluginManager, DataMaskingService $dataMaskingService, EntityTypeBundleInfoInterface $entityTypeBundleInfo, EntityFieldManagerInterface $entityFieldManager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dataMaskingPluginManager = $dataMaskingPluginManager;
    $this->dataMaskingService = $dataMaskingService;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.data_masking_plugin'),
      $container->get('data_masking.service'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['displays'] = [];
    $input = &$form_state->getUserInput();
    // Create the part of the form that allows the user to select the basic
    // properties of what the entity to delete.
    $form['displays']['show'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity Masking Settings'),
      '#tree' => TRUE,
    ];
    $content_entity_types = [];
    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      if ($definition instanceof ContentEntityType) {
        $content_entity_types[$definition->id()] = "{$definition->getLabel()} ({$definition->id()})";
      }
    }
    $form['displays']['show']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $content_entity_types,
      '#empty_option' => $this->t('-select-'),
      '#size' => 1,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxCallChangeEntity'],
      ],
    ];

    $type_options = [];
    $form['displays']['show']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => $type_options,
      '#required' => TRUE,
      '#prefix' => '<div id="entity-type-wrapper">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'ajaxCallChangeBundle'],
      ],
    ];

    $field_options = [];
    $form['displays']['show']['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => $field_options,
      '#required' => TRUE,
      '#prefix' => '<div id="entity-field-wrapper">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'ajaxCallChangeField'],
      ],
    ];

    $field_properties = [];
    $form['displays']['show']['property'] = [
      '#type' => 'select',
      '#title' => $this->t('Property'),
      '#options' => $field_properties,
      '#required' => TRUE,
      '#prefix' => '<div id="entity-property-wrapper">',
      '#suffix' => '</div>',
    ];

    $masking_methods = [];
    $form['displays']['show']['masking_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Masking Method'),
      '#options' => $masking_methods,
      '#required' => TRUE,
      '#prefix' => '<div id="masking-method-wrapper">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'ajaxCallChangeMaskingMethod'],
      ],
    ];

    $masking_schemes = [];
    $form['displays']['show']['masking_scheme'] = [
      '#type' => 'select',
      '#title' => $this->t('Masking Scheme'),
      '#options' => $masking_schemes,
      '#required' => TRUE,
      '#prefix' => '<div id="masking-scheme-wrapper">',
      '#suffix' => '</div>',
    ];

    if (isset($input['show']['entity_type'])) {
      $default_bundles = $this->entityTypeBundleInfo
        ->getBundleInfo($input['show']['entity_type']);
      /*If the current base table support bundles and has more than one (like user).*/
      if (!empty($default_bundles)) {
        // Get all bundles and their human readable names.
        foreach ($default_bundles as $type => $bundle) {
          $type_options[$type] = "{$bundle['label']} ($type)";
        }
        $form['displays']['show']['type']['#options'] = $type_options;
      }

      if (!empty($input['show']['type'])) {
        $entity_field_definitions = $this->entityFieldManager->getFieldDefinitions($input['show']['entity_type'], $input['show']['type']);
        $allowedFieldTypes = $this->dataMaskingPluginManager->getExistingMaskingFieldTypes();
        foreach ($entity_field_definitions as $field_id => $entity_field_definition) {
          if (in_array($entity_field_definition->getType(), $allowedFieldTypes)) {
            $field_options[$field_id] = "{$entity_field_definition->getLabel()} ($field_id)";
          }
        }
        $form['displays']['show']['field']['#options'] = $field_options;

        if (!empty($input['show']['field'])
          && isset($entity_field_definitions[$input['show']['field']])) {
          $entity_field_definition = $entity_field_definitions[$input['show']['field']];
          $property_definitions = [];
          if (is_callable([
            $entity_field_definitions[$input['show']['field']],
            'getPropertyDefinitions',
          ]) && $entity_field_definition instanceof FieldStorageDefinitionInterface) {
            $property_definitions = $entity_field_definition->getPropertyDefinitions();
          }
          elseif (is_callable([
            $entity_field_definitions[$input['show']['field']],
            'getFieldStorageDefinition',
          ])) {
            $property_definitions = $entity_field_definition->getFieldStorageDefinition()->getPropertyDefinitions();
          }
          foreach ($property_definitions as $property_id => $property_definition) {
            $field_properties[$property_id] = "{$property_definition->getLabel()} ($property_id)";
          }
          $form['displays']['show']['property']['#options'] = $field_properties;

          $masking_plugins = $this->dataMaskingPluginManager->getDefinitionByAllowedFieldType($entity_field_definition->getType());
          foreach ($masking_plugins as $masking_plugin) {
            $masking_methods[$masking_plugin['id']] = "{$masking_plugin['label']} ({$masking_plugin['id']})";
          }
          $form['displays']['show']['masking_method']['#options'] = $masking_methods;

          if (!empty($input['show']['masking_method'])
            && isset($masking_methods[$input['show']['masking_method']])) {
            $masking_plugin = $this->dataMaskingPluginManager->getDefinition($input['show']['masking_method']);
            if (!empty($masking_plugin)) {
              foreach ($masking_plugin['masking_schemes'] as $scheme => $schemeLabel) {
                $masking_schemes[$scheme] = "{$schemeLabel} ($scheme)";
              }
              $form['displays']['show']['masking_scheme']['#options'] = $masking_schemes;
            }
          }
        }
      }
    }

    $form['batch_size'] = [
      '#type' => 'number',
      '#max' => 100,
      '#min' => 1,
      '#default_value' => 20,
      '#required' => TRUE,
      '#title' => $this->t('Batch size'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Data Masking'),
    ];

    return $form;
  }

  /**
   * Change entity ajax callback.
   */
  public function ajaxCallChangeEntity(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#entity-type-wrapper', $form['displays']['show']['type']));
    $response->addCommand(new ReplaceCommand('#entity-field-wrapper', $form['displays']['show']['field']));
    $response->addCommand(new ReplaceCommand('#entity-property-wrapper', $form['displays']['show']['property']));
    $response->addCommand(new ReplaceCommand('#masking-method-wrapper', $form['displays']['show']['masking_method']));
    $response->addCommand(new ReplaceCommand('#masking-scheme-wrapper', $form['displays']['show']['masking_scheme']));
    return $response;
  }

  /**
   * Change bundle ajax callback.
   */
  public function ajaxCallChangeBundle(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#entity-field-wrapper', $form['displays']['show']['field']));
    $response->addCommand(new ReplaceCommand('#entity-property-wrapper', $form['displays']['show']['property']));
    $response->addCommand(new ReplaceCommand('#masking-method-wrapper', $form['displays']['show']['masking_method']));
    $response->addCommand(new ReplaceCommand('#masking-scheme-wrapper', $form['displays']['show']['masking_scheme']));
    return $response;
  }

  /**
   * Change field ajax callback.
   */
  public function ajaxCallChangeField(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#entity-property-wrapper', $form['displays']['show']['property']));
    $response->addCommand(new ReplaceCommand('#masking-method-wrapper', $form['displays']['show']['masking_method']));
    $response->addCommand(new ReplaceCommand('#masking-scheme-wrapper', $form['displays']['show']['masking_scheme']));
    return $response;
  }

  /**
   * Change masking method ajax callback.
   */
  public function ajaxCallChangeMaskingMethod(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#masking-scheme-wrapper', $form['displays']['show']['masking_scheme']));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch_size = $form_state->getValue('batch_size');
    $entity_type = $form_state->getValue(['show', 'entity_type']);
    $bundle = $form_state->getValue(['show', 'type']);
    $field = $form_state->getValue(['show', 'field']);
    $property = $form_state->getValue(['show', 'property']);
    $masking_method = $form_state->getValue(['show', 'masking_method']);
    $masking_scheme = $form_state->getValue(['show', 'masking_scheme']);

    $batch = $this->dataMaskingService->buildBatch($batch_size, [
      [
        DataMaskingService::ENTITY_TYPE => $entity_type,
        DataMaskingService::BUNDLE => $bundle,
        DataMaskingService::FIELD => $field,
        DataMaskingService::PROPERTY => $property,
        DataMaskingService::MASKING_METHOD => $masking_method,
        DataMaskingService::MASKING_SCHEME => $masking_scheme,
      ],
    ]);

    if (empty($batch)) {
      $this->messenger->addMessage("Not found entity to process!");
    }
    else {
      batch_set($batch);
    }
  }

}
